import { useState } from "react";
import { fetchRequest } from "./redux/actions";
import { connect } from "react-redux";

function App(props) {
  const [couponValue, setValue] = useState();
  
  return (
    <div className="App">
      <div
        className={
          "coupon-box " +
          (props.couponResponse ? props.couponResponse.status : "")
        }
      >
        <p>Have a coupon/referral code? Apply</p>
        <div className="input-wrapper">
          <form onSubmit={(e) => e.preventDefault()}>
            <input type="text" onChange={(e) => setValue(e.target.value)} />
            <button onClick={() => props.fetchCoupon(couponValue)}>
              Apply
            </button>
            {props.loading ? (
              <p>Please wait!</p>
            ) : props.couponResponse ? (
              <p className="msg">{props.couponResponse.message}</p>
            ) : (
              ""
            )}
          </form>
        </div>
      </div>
    </div>
  );
}

function mapState(state) {
  return state;
}

function mapDispatch(dispatch) {
  return {
    fetchCoupon: (id) => dispatch(fetchRequest(id)),
  };
}

export default connect(mapState, mapDispatch)(App);
